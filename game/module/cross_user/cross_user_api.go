package cross_user

import (
	"gogame/game/module/test"
	"gogame/pb"
)

func ApiGetInfo() {
	Args := &pb.UserGetInfoArgs{
		Uid: "0_62df56b8ac9a982e585b697f",
	}
	CrossUser.GetInfo(test.Request, Args, test.Response)
}
