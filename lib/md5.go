package lib

import (
	"crypto/md5"
	"encoding/hex"
)

// Md5 Md5加密
func Md5(str string) string {
	_md5 := md5.New()
	_md5.Write([]byte(str))
	_byte := _md5.Sum(nil)
	return hex.EncodeToString(_byte)
}
