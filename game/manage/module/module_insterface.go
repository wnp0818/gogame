package module

import (
	"go.mongodb.org/mongo-driver/bson"
	"gogame/module_option/taskoption"
	"gogame/pb"
)

type (
	// CrossUserInterface 跨服user模块接口
	CrossUserInterface interface {
	}

	// UserInterface 用户模块接口
	UserInterface interface {
		// CreateUser 创建玩家
		CreateUser(bindUid, ip string, sid int64, gatewayUrl string) *pb.ModelUser
		// GetUserInfoByWhere 通过条件查询玩家
		GetUserInfoByWhere(where bson.M) *pb.ModelUser
	}

	// HeroInterface 英雄模块接口
	HeroInterface interface {
		// GetDefHeroInfo 获取初始英雄数据
		GetDefHeroInfo(uid string, hid string) *pb.ModelHero
	}

	// ItemInterface 物品模块接口
	ItemInterface interface {
	}

	// ChatInterface 聊天模块接口
	ChatInterface interface {
		// SendMessage 发送聊天消息
		SendMessage(uid string, msgInfo *pb.SendMsgArgs)
	}

	// TaskInterface 任务模块接口
	TaskInterface interface {
		// FmtTaskData 格式化任务数据
		FmtTaskData(taskId int32, option taskoption.FmtTaskData) *pb.ModelTask
		// SetTaskVal 设置任务数据
		SetTaskVal(uid string, sType int32, val int32, chkVal any, IsInc bool)
	}
)
