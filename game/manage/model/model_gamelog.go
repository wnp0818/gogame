package model

import (
	cfg "gogame/game/manage/config/structs"
)

type GameLog struct {
	*ModelInfo
}

func init() {
	model := &GameLog{
		ModelInfo: NewModelInfo("gamelog", "_id", WriteRedis(false)),
	}
	Manage.models.GameLog = model
}

// Add 增加日志
func (self *GameLog) Add(uid, act string, data []*cfg.Gameatn, logData map[string]any) {
	_data := make(map[string]any)
	for k, v := range logData {
		if k == "act" {
			continue
		}
		_data[k] = v
	}
	_data[act] = data
	_insertData := map[string]any{
		"type":    logData["act"],
		"ctime":   C.Now(),
		"uid":     uid,
		"data":    _data,
		"_id":     C.GetObjectId(),
		"ttltime": C.TTLTime(),
	}

	self.InsertOne("system", _insertData)

}
