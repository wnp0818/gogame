package user

import (
	"go.mongodb.org/mongo-driver/bson"
	g "gogame/game"
	"gogame/pb"
	"gogame/server/rpcx"
)

func (h *Api) ChangeNameCheck(args *pb.ChangeNameArgs, res *rpcx.Response) {
	res.IsCheck = 1
	res.S = 1

	_userInfo := g.M.User.GetUserInfoByWhere(bson.M{"name": args.Name})
	// 名字已存在
	if _userInfo.Uid != "" {
		res.S = -1
		res.ErrorMsg = g.L("user_error_2")
	}

}

// ChangeName 改名
func (h *Api) ChangeName(request *rpcx.Request, args *pb.ChangeNameArgs, res *rpcx.Response) {
	h.ChangeNameCheck(args, res)
	// 条件检测不通过
	if res.S != 1 {
		return
	}

	// 修改玩家名
	_setData := map[string]any{
		"name": args.Name,
	}
	g.DATA.UserInfo.UpdateUserInfo(request.Uid, _setData)

}
