package model

import (
	"gogame/pb"
)

type Hero struct {
	*ModelInfo
}

func init() {
	model := &Hero{
		ModelInfo: NewModelInfo("hero", "_id"),
	}
	model.ModelInfo.Child = model
	Manage.models.Hero = model
}

// DataChange 数据改变
func (self *Hero) DataChange(uid, act string, datas ...any) {
}

// AddHero 增加英雄
func (self *Hero) AddHero(uid string, hidList ...string) map[string]*pb.ModelHero {
	var (
		_anyList  []any
		_oid2Hero = make(map[string]*pb.ModelHero)
	)
	for _, hid := range hidList {
		_heroInfo := M.Hero.GetDefHeroInfo(uid, hid)
		// 英雄不存在
		if _heroInfo == nil {
			continue
		}
		_objectId := C.GetObjectId()
		_heroInfo.Id = _objectId
		_heroInfo.Lt = C.Now()
		_anyList = append(_anyList, _heroInfo)
		_oid2Hero[_objectId] = _heroInfo
	}

	self.InsertMany(uid, _anyList)

	return _oid2Hero

}

// GetList 获取玩家英雄列表
func (self *Hero) GetList(uid string) []*pb.ModelHero {
	var heroList []*pb.ModelHero
	self.ModelInfo.GetList(uid, &heroList)

	return heroList
}

// GetInfo 获取英雄数据
func (self *Hero) GetInfo(id string) *pb.ModelHero {
	heroInfo := new(pb.ModelHero)
	self.Get(id, heroInfo)

	return heroInfo
}
