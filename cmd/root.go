package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
)

var RootCmd = &cobra.Command{
	Use:   "wait run",
	Short: "命令行",
	Long:  "命令行工具",
	Args: func(cmd *cobra.Command, args []string) error {
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("wait run")
	},
}

func Execute() {
	_ = RootCmd.Execute()
}
