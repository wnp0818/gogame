package main

import (
	g "gogame/game"
	"gogame/game/module"
	"gogame/gameconfig"
	"gogame/gameservice"
	_ "gogame/gameservice/gateway"
	_ "gogame/gameservice/modellog"
	_ "gogame/gameservice/timer"
	_ "gogame/gameservice/worker"
	"time"
)

func main() {
	// 接口注册
	module.InitRoute()

	// 启动worker
	gameservice.RunWorker()

	time.Sleep(time.Second * 2)

	// 启动定时器
	gameservice.RunTimer()
	// 启动model_log
	gameservice.RunModelLog()

	_gameVer := gameconfig.ServerConfig.GetGameVer()
	switch _gameVer {
	case "cross":
		g.CrossRedis.FlushAll()
	default:
		g.Redis.FlushAll()

		// 启动gateway
		gameservice.RunGateway()
	}

	// 等待所有服务结束
	gameservice.WaitStop()

}
