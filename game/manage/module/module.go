package module

/*
module管理 方便所有模块供模块间相互调用
*/

// ManageStruct 模块这里使用接口，是为了避免循环引用的问题(要引入各模块，各模块又需要引入manage) emm，暂时没想到更好方案
type ManageStruct struct {
	// 跨服模块
	CrossUser CrossUserInterface

	// 本服模块
	User UserInterface
	Hero HeroInterface
	Item ItemInterface
	Chat ChatInterface
	Task TaskInterface
}

var Manage *ManageStruct = &ManageStruct{}
