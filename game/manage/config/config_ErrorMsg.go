package config

type ErrorMsg struct {
    *JsonHandle
    // 预处理字段定义区域---start

    // 预处理字段定义区域---end
}

func init() {
    config := &ErrorMsg{
        JsonHandle: NewJsonHandle("ErrorMsg"),
    }
    Manage.configs.ErrorMsg = config
    RegisterConfigInterface(config.Name, config)
}

// Load 加载配置
func (self *ErrorMsg) Load(jsonData any) error {
    err := self.JsonHandle.Load(jsonData)
    if err != nil {
        return err
    }
    self.PreConfig()
    return nil
}

// PreConfig 配置预处理
func (self *ErrorMsg) PreConfig() {

}

// ------------------------ 以上文件内容自动生成,请勿随便修改! ------------------------
// ------------------------ 下方、定义配置diy方法 ------------------------

// GetErrorMsg 获取错误消息
func (self *ErrorMsg) GetErrorMsg(errorMsgId string) string {
    _errMsg, _ok := self.JsonData[errorMsgId]
    if !_ok {
        return ""
    }
    return _errMsg.(string)
}