package chat

import (
	g "gogame/game"
	"gogame/game/manage/module"
	"gogame/game/module/base"
	"gogame/gameservice/worker"
	"gogame/pb"
)

type ModuleChat struct {
	*base.ModuleBase
	Api
}

var Chat *ModuleChat

func init() {
	Chat = &ModuleChat{
		ModuleBase: base.NewModuleBase(),
	}

	_moduleName := "chat"
	// 路由注册 未定义Api2Func默认采用反射筛选符合条件的方法注册和调用
	worker.Register(_moduleName, Chat)
	// 模块注册
	module.Manage.Chat = Chat

}

// SendMessage 发送聊天消息 1系统聊天 2世界聊天 3私聊 4公会聊天 5跨服聊天 6跑马灯
func (m *ModuleChat) SendMessage(uid string, msgInfo *pb.SendMsgArgs) {
	_content := msgInfo.Content
	_toUid := msgInfo.ToUid
	_msgType := msgInfo.MsgType

	// 敏感词替换
	_content = g.FilterMsg(_content)

	_sendMsg := map[string]any{
		"content": _content,
		"msgtype": _msgType,
		"touid":   _toUid,
		"ctime":   g.C.Now(),
		"uid":     uid,
	}

	switch _msgType {
	case 5:
		// 跨服消息
		g.SendCrossMsg(_sendMsg)
	default:
		// 发送给所有人
		if _toUid == "" {
			g.SendMsgByAll(_sendMsg)
		} else {
			// 发送给指定玩家
			g.SendMsgByUid(_toUid, _sendMsg)
			// 推送给自己
			g.SendMsgByUid(uid, _sendMsg)
		}
	}

}
