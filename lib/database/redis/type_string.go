package redis

import (
	"github.com/go-redis/redis/v8"
	"time"
)

// Del 删除key
func (r *RedisStruct) Del(key string) *redis.IntCmd {
	return r.Client.Del(r.Context(), key)
}

// Get 获取值
func (r *RedisStruct) Get(key string) *redis.StringCmd {
	return r.Client.Get(r.Context(), key)
}

// Set 设置value
func (r *RedisStruct) Set(key string, val any, expiration time.Duration) *redis.StatusCmd {
	return r.Client.Set(r.Context(), key, val, expiration)
}
