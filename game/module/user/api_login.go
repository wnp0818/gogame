package user

import (
	"go.mongodb.org/mongo-driver/bson"
	g "gogame/game"
	"gogame/logger"
	"gogame/pb"
	"gogame/server/rpcx"
)

func (h *Api) LoginCheck(args *pb.LoginArgs, res *rpcx.Response) {
	res.IsCheck = 1
	res.S = 1

	_sid := args.Sid
	// 区服id错误
	if !g.Config.HasSid(_sid) {
		res.S = -1
		res.ErrorMsg = g.L("user_error_1")
		return
	}

}

// Login 登陆
func (h *Api) Login(request *rpcx.Request, args *pb.LoginArgs, res *rpcx.Response) {
	h.LoginCheck(args, res)
	if res.S != 1 {
		return
	}

	_bindUid := args.BindUid
	_sid := args.Sid
	_gatewayUrl := request.GatewayUrl

	var isNew bool
	_gud := &pb.ModelUser{}
	g.Mdb.FindOneByReflect("userinfo", bson.M{"binduid": _bindUid, "sid": _sid}, _gud)
	// 玩家不存在，创建玩家
	if _gud.Uid == "" {
		isNew = true
		_gud = g.M.User.CreateUser(_bindUid, "", _sid, _gatewayUrl)
	} else {
		g.DATA.UserInfo.InitUserInfo(_gud)
	}

	_uid := _gud.Uid

	// 踢掉上一个登陆的玩家
	_fGatewayUrl := g.GetUidGatewayUrl(_uid)
	if _fGatewayUrl != "" {
		g.RpcDo(
			nil,
			map[string]string{
				"uid":          _uid,
				"othergateway": _gatewayUrl,
			},
			"OtherLogin",
			_fGatewayUrl,
		)
	}

	logger.Print("【%s】%s 登陆成功", _gud.BindUid, _uid)

	// 设置玩家当前gateway
	g.SetUidGatewayUrl(_uid, _gatewayUrl)

	// 检测生成任务
	g.DATA.Task.ChkGenerateTask(_uid)

	// 设置最后一次登录时的ip、时间
	_setData := map[string]any{
		"lastloginip": request.Ip,
		"logintime":   g.C.Now(),
	}
	if !isNew {
		// 累计登陆天数
		if _gud.Lt < g.C.Zero(g.C.Now()) {
			_setData["loginnum"] = _gud.LoginNum + 1
		}
	}
	g.DATA.UserInfo.UpdateUserInfo(_uid, _setData)

	// 同步玩家跨服数据
	go g.DATA.CrossUserInfo.PushGud(_uid, _gud)

	// 用于ws绑定uid
	res.Uid = _uid

	// 消息提前返回测试，提前把gud返回
	//res.SendFirstProtoMsg(
	//	"user.GetInfo",
	//	&pb.UserGetInfoRes{UserInfo: _gud},
	//)

	// 返回值
	res.SendMsg(&pb.LoginRes{
		Gud:       _gud,
		OpenDay:   g.Config.GetOpenDay(), // 开服天数
		WorkerUrl: request.WorkerUrl,     // 返回当前workerUrl便于调式
	})

}
