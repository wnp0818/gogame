package gameconfig

import "gogame/lib"

type Config struct {
	LocalIp    string                `json:"localip"`    // 本机ip
	Worker     []string              `json:"worker"`     // worker地址
	Gateway    []string              `json:"gateway"`    // gateway地址
	EtcdUrl    []string              `json:"etcdurl"`    // etcd地址
	MongoDb    MongoDb               `json:"mongodb"`    // mongodb配置
	Redis      Redis                 `json:"redis"`      // redis配置
	ServerInfo map[string]ServerInfo `json:"serverinfo"` // 区服信息
	OpenTime   string                `json:"opentime"`   // 区服开服时间
	Ver        string                `json:"ver"`        // 游戏版本
	CrossVer   string                `json:"crossver"`   // 跨服版本
	GameKey    string                `json:"gamekey"`    // 游戏唯一key
	Sid        int64                 // 区服主sid
}

var ServerConfig = LoadServerConfig()

// GetSid 获取区服主sid
func (c Config) GetSid() int64 {
	var _minSid int64 = 10000000000
	for sid, _ := range c.ServerInfo {
		_sid := lib.Convert.String2Int(sid)
		if _sid < _minSid {
			_minSid = _sid
		}
	}

	return _minSid
}

// HasSid 当前区服是否存在该sid
func (c Config) HasSid(sid int64) bool {
	_, ok := c.ServerInfo[lib.Convert.Int64ToString(sid)]
	if ok {
		return true
	}

	return false
}

// GetGameVer 获取游戏版本
func (c Config) GetGameVer() string {
	return c.Ver
}

// GetOpenTime 获取开服时间
func (c Config) GetOpenTime() int64 {
	return lib.Time.Date2Now(c.OpenTime)
}

// GetOpenDay 获取开服了多少天
func (c Config) GetOpenDay() int64 {
	_diffTime := lib.Time.Now() - c.GetOpenTime()
	_diffDay := _diffTime / (24 * 3600)

	return _diffDay
}
