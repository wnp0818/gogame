package task

import (
	g "gogame/game"
	cfg "gogame/game/manage/config/structs"
	"gogame/pb"
	"gogame/server/rpcx"
)

func (h *Api) FinishCheck(args *pb.TaskFinishArgs, res *rpcx.Response) (*cfg.GameTaskData, *pb.ModelTask) {
	res.S = 1
	res.IsCheck = 1

	oid := args.Oid
	_taskInfo := g.DATA.Task.GetInfo(oid)
	// 任务不存在
	if _taskInfo.Uid == "" {
		res.S = -1
		res.ErrorMsg = g.L("task_error_3")
		return nil, nil
	}

	_taskCon := g.GC.Task().Get(_taskInfo.TaskId)
	// 没有这个任务
	if _taskCon == nil {
		res.S = -2
		res.ErrorMsg = g.L("task_error_2")
		return nil, nil
	}

	// 任务已完成
	if _taskInfo.Finish == 1 {
		res.S = -3
		res.ErrorMsg = g.L("task_error_4")
		return nil, nil
	}

	// 条件未完成无法领取
	if _taskInfo.NVal < _taskCon.Pval {
		res.S = -4
		res.ErrorMsg = g.L("task_error_5")
		return nil, nil
	}

	return _taskCon, _taskInfo
}

// Finish 完成任务
func (h *Api) Finish(request *rpcx.Request, args *pb.TaskFinishArgs, res *rpcx.Response) {
	_taskCon, _taskInfo := h.FinishCheck(args, res)
	if res.S != 1 {
		return
	}

	uid := request.Uid

	// 设置任务已完成
	_setData := map[string]any{
		"finish": 1,
	}
	g.DATA.Task.Set(uid, _taskInfo.Id, _setData)

	// 获取奖励
	_prize, _changInfo := g.GetPrizeRes(uid, _taskCon.Prize, map[string]any{"act": "任务-领取任务奖励"}, request.GatewayUrl, false)
	g.SendConnChangeInfo(res, _changInfo)

	res.SendMsg(&pb.TaskFinishRes{
		Prize: _prize,
	})
}
