package gameconfig

import (
	"fmt"
	"gogame/lib"
	"gogame/logger"
)

type Redis struct {
	PassWord    string   `json:"password"`  // 密码
	AddressList []string `json:"addresss"`  // redis集群地址
	Address     string   `json:"address"`   // redis单机地址
	IsCluster   bool     `json:"iscluster"` // 是否集群
}

type MongoDb struct {
	Url      string `json:"url"`      // mongo地址
	PoolSize uint64 `json:"poolsize"` // 连接池大小
	DbName   string `json:"dbname"`   // 数据库名
}

type ServerInfo struct {
	ServerName string `json:"servername"` // 区服名
	ServerId   int64  `json:"serverid"`   // 区服sid
}

// LoadServerConfig 加载项目配置
func LoadServerConfig() Config {
	var config Config
	jsonData := lib.ReadFile(lib.GetFilePath("config.json"))
	err := lib.Loads(jsonData, &config)
	if err != nil {
		logger.WorkerLog.Warn(fmt.Sprintf("LoadServerConfig fail\nerr--->%s", err.Error()))
	}
	config.Sid = config.GetSid()
	return config

}

// LoadCrossServerConfig 加载跨服配置
func LoadCrossServerConfig() CrossServerConfig {
	_crossServerMap := make(map[string]CrossServerConfig)
	jsonData := lib.ReadFile(lib.GetFilePath("crossserver.json"))
	err := lib.Loads(jsonData, &_crossServerMap)
	if err != nil {
		logger.WorkerLog.Warn(fmt.Sprintf("LoadCrossServerConfig fail\nerr--->%s", err.Error()))
	}
	crossServerConfig, ok := _crossServerMap[ServerConfig.CrossVer]
	if !ok {
		logger.WorkerLog.Warn(fmt.Sprintf("【%s】对应的跨服配置缺失!!!", ServerConfig.CrossVer))
		return crossServerConfig
	}

	return crossServerConfig

}
