package lib

import (
	"encoding/base64"
	"fmt"
)

// Base64Encode base64加密
func Base64Encode(data string) string {
	return base64.StdEncoding.EncodeToString([]byte(data))
}

// Base64Decode base64解密
func Base64Decode(data string) string {
	_byte, err := base64.StdEncoding.DecodeString(data)
	if err != nil {
		fmt.Printf("Base64Decode fail! data-->%s\n", data)
	}

	return string(_byte)
}

// UrlBase64Encode url base64加密
func UrlBase64Encode(data string) string {
	return base64.URLEncoding.EncodeToString([]byte(data))
}

// UrlBase64Decode url base64解密
func UrlBase64Decode(data string) string {
	_byte, err := base64.URLEncoding.DecodeString(data)
	if err != nil {
		fmt.Printf("UrlBase64Decode fail! data-->%s\n", data)
	}

	return string(_byte)
}
