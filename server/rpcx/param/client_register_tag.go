package param

type ClientArgs struct {
	Tag       string
	WorkerUrl string
}

type ClientRes struct {
	S uint16
}
