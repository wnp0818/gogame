package model

import (
	"fmt"
	myredis "gogame/lib/database/redis"
	"gogame/logger"
)

type ModelBase struct {
	Redis      *myredis.RedisStruct
	DbName     string // mongodb表名
	UniKey     string // 唯一key
	Ver        string // 跨服还是本服
	ExpireTime int64  // 过期时间int64
	LastTime   int64  // 最后一次访问model的时间
	WriteRedis bool   // 数据是否写入redis
}

func GenerateModelBase(modelBase *ModelBase) *ModelBase {
	return &ModelBase{
		Redis:      modelBase.Redis,
		DbName:     modelBase.DbName,
		UniKey:     modelBase.UniKey,
		Ver:        modelBase.Ver,
		ExpireTime: modelBase.ExpireTime,
		LastTime:   C.Now(),
	}
}

// OnlyKey mongodb table表redis缓存key
func (m *ModelBase) OnlyKey(key string) string {
	return fmt.Sprintf("%s_%s", m.DbName, key)
}

// AddModelBase 数据载入redis时同步到Model用作数据清除
func (m *ModelBase) AddModelBase(uid string) {
	Manage.AddModelBase(uid, m)
}

// UpdateLastTime 更新model访问时间
func (m *ModelBase) UpdateLastTime(uid string) {
	Manage.UpdateLastTime(uid, m.DbName)
}

// CheckExpire 检测model是否过期
func (m *ModelBase) CheckExpire(nt int64) bool {
	if (nt - m.LastTime) > m.ExpireTime {
		return true
	}
	return false
}

// Clear 数据清理
func (m *ModelBase) Clear(uid string) {
	_key := m.OnlyKey(uid)
	switch m.UniKey {
	// 直接删除
	case "uid":
		m.Redis.Pipeline.Del(_key)
	// 先查出所有元素，再删除
	case "_id":
		uniKey2RedisDataKey, err := m.Redis.Client.HGetAll(m.Redis.Context(), _key).Result()
		// redis err or 为空
		if err != nil || len(uniKey2RedisDataKey) == 0 {
			return
		}

		m.Redis.Pipeline.Del(_key)
		for _, redisDataKey := range uniKey2RedisDataKey {
			m.Redis.Pipeline.Del(redisDataKey)
		}

	}
	logger.Print("%s【%s】过期清除~~~", uid, m.DbName)
}
