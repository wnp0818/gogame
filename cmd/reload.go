package cmd

import (
	"context"
	"github.com/spf13/cobra"
	"gogame/server/rpcx/client"
	"gogame/server/rpcx/param"
)

/*
	热更
*/

var ReloadCmd = &cobra.Command{
	Use: "reload",
	Run: Reload,
}

func Reload(cmd *cobra.Command, reloadFiles []string) {
	_rpcxClient := client.NewRpcXClient("cmd_reload", "api", nil)
	_rpcxClient.Dial(false)
	_rpcxClient.Fork(
		context.Background(),
		"Reload",
		reloadFiles,
		&param.AnyRes{},
	)
	_rpcxClient.Close()
}

func init() {
	RootCmd.AddCommand(ReloadCmd)
}
