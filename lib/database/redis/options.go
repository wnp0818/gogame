package redis

import (
	"gogame/gameconfig"
)

// InitOptions 初始化redis配置
func InitOptions(ver string) gameconfig.Redis {
	switch ver {
	case "cross":
		return gameconfig.CrossServer.Redis
	default:
		return gameconfig.ServerConfig.Redis
	}

}
