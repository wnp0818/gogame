package procfunc

import (
	"gogame/server/rpcx"
)

type ProcFunc func(request *rpcx.Request, args any, res *rpcx.Response)
