var chat_api_pb = require("./pb/pb_js/chat/chat_api_pb")
var common_pb = require("./pb/pb_js/common/common_pb")
var hero_api_pb = require("./pb/pb_js/hero/hero_api_pb")
var hero_mongo_pb = require("./pb/pb_js/hero/hero_mongo_pb")
var item_api_pb = require("./pb/pb_js/item/item_api_pb")
var item_mongo_pb = require("./pb/pb_js/item/item_mongo_pb")
var rpcx_param_pb = require("./pb/pb_js/rpcx/rpcx_param_pb")
var task_api_pb = require("./pb/pb_js/task/task_api_pb")
var task_mongo_pb = require("./pb/pb_js/task/task_mongo_pb")
var user_api_pb = require("./pb/pb_js/user/user_api_pb")
var user_mongo_pb = require("./pb/pb_js/user/user_mongo_pb")

module.exports = {
    chat_api_pb: chat_api_pb,
    common_pb: common_pb,
    hero_api_pb: hero_api_pb,
    hero_mongo_pb: hero_mongo_pb,
    item_api_pb: item_api_pb,
    item_mongo_pb: item_mongo_pb,
    rpcx_param_pb: rpcx_param_pb,
    task_api_pb: task_api_pb,
    task_mongo_pb: task_mongo_pb,
    user_api_pb: user_api_pb,
    user_mongo_pb: user_mongo_pb,
}