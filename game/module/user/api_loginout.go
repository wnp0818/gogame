package user

import (
	g "gogame/game"
	"gogame/logger"
	"gogame/pb"
	"gogame/server/rpcx"
)

func (h *Api) LoginOutCheck(args *pb.LoginOutArgs, res *rpcx.Response) {
	res.IsCheck = 1
}

// LoginOut 登出
func (h *Api) LoginOut(request *rpcx.Request, args *pb.LoginOutArgs, res *rpcx.Response) {
	h.LoginOutCheck(args, res)

	_uid := request.Uid

	// 清除玩家当前gateway
	g.DelUidGatewayUrl(_uid)
	// 通知保存玩家数据
	g.NoticeSaveUserData(_uid)
	// 清除玩家redis数据
	g.DATA.ClearModelByUid(_uid)

	logger.Print("【%s】 玩家登出", _uid)

}
