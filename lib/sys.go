package lib

import (
	"fmt"
	uuid "github.com/satori/go.uuid"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"strconv"
)

type SysStruct struct {
}

var Sys SysStruct

func init() {
	Sys = SysStruct{}
}

// GetUUid 获取一个uuid
func (s SysStruct) GetUUid() string {
	return uuid.NewV4().String()
}

// GetUUidByMongoId 根据mongo的_id生成一个uuid
func (s SysStruct) GetUUidByMongoId(id string) string {
	code := string([]byte(id)[18:])
	_intCode, _err := strconv.ParseInt(code, 16, 64)
	if _err != nil {
		fmt.Printf("String2Int error-->: %s\n", _err)
	}
	_strCode := strconv.Itoa(int(_intCode))
	return string([]byte(_strCode)[1:])
}

// GetObjectId 获取一个mongo的_id
func (s SysStruct) GetObjectId() string {
	return primitive.NewObjectID().Hex()
}
