package model

import (
	"gogame/module_option/taskoption"
	"gogame/pb"
)

type Task struct {
	*ModelInfo
}

func init() {
	model := &Task{
		ModelInfo: NewModelInfo("task", "_id"),
	}
	model.ModelInfo.Child = model
	Manage.models.Task = model
}

// DataChange 数据改变
func (self *Task) DataChange(uid, act string, datas ...any) {
}

// SetAddTask 增加任务
func (self *Task) SetAddTask(uid string, taskId int32, option taskoption.SetAddTask) *pb.ModelTask {
	_insertData := option.InsertData
	if _insertData == nil {
		_insertData = M.Task.FmtTaskData(
			taskId,
			taskoption.FmtTaskData{
				TaskCon: option.TaskCon,
				Uid:     uid,
			},
		)
	}

	if option.IsInsert {
		self.insertOne(uid, _insertData, false)
	}

	return _insertData
}

// generateTask 生成任务
func (self *Task) generateTask(uid string, taskType ...int32) {
	var toTaskType int32
	if len(taskType) > 0 {
		toTaskType = taskType[0]
	}
	_initTaskList := GC.Task().InitTaskList
	_insertTaskList := make([]any, 0)
	_nt := C.Now()
	for _, task := range _initTaskList {
		if toTaskType > 0 {
			// 任务类型不一致
			if task.Type != toTaskType {
				continue
			}

			_task := &pb.ModelTask{
				TaskId: task.Taskid,
				Type:   task.Type,
				SType:  task.Stype,
				NVal:   0,
				Uid:    uid,
				Id:     C.GetObjectId(),
				Ctime:  _nt,
				ReTime: _nt,
			}

			_insertTaskList = append(_insertTaskList, _task)
		}

		// 新增任务
		if len(_insertTaskList) > 0 {
			self.InsertMany(uid, _insertTaskList)
		}

	}
}

// GetTaskType2Flag 获取任务类型对应的刷新key
func (self *Task) GetTaskType2Flag() map[int32]string {
	return map[int32]string{
		3: "day",
	}
}

// refreshTask 刷新任务  day-每日刷新 week-每周刷新
func (self *Task) refreshTask(uid string) {
	_taskType2Flag := self.GetTaskType2Flag()
	_taskList := self.GetList(uid)
	_taskType2TaskList := make(map[int32][]*pb.ModelTask)
	for _, task := range _taskList {
		_taskType := task.Type
		_, ok := _taskType2Flag[_taskType]
		// 无需刷新
		if !ok {
			continue
		}
		_taskType2TaskList[_taskType] = append(_taskType2TaskList[_taskType], task)
	}

	// 没有需要刷新的任务
	if len(_taskType2TaskList) == 0 {
		return
	}

	_nt := C.Now()
	_zero := C.Zero(_nt)
	_curWeek := C.GetWeekKey()
	for taskType, flag := range _taskType2Flag {
		_chkTaskList := _taskType2TaskList[taskType]
		for _, task := range _chkTaskList {
			_reTime := task.ReTime
			switch flag {
			// 每日重置
			case "day":
				// 还没到重置时间
				if _zero < _reTime {
					continue
				}
			// 每周重置
			case "week":
				// 还没到重置时间
				if C.GetWeekKey(_reTime) != _curWeek {
					continue
				}
			}

			_setData := map[string]any{
				"nval":   0,
				"finish": 0,
				"retime": _nt,
			}
			self.Set(uid, task.Id, _setData)

		}
	}

}

// ChkGenerateTask 检测生成任务
func (self *Task) ChkGenerateTask(uid string) {
	// 没有任务生成任务
	if self.GetList(uid) == nil {
		self.generateTask(uid)
	}

	// 检测任务刷新
	self.refreshTask(uid)

}

// GetList 获取玩家所有任务
func (self *Task) GetList(uid string) []*pb.ModelTask {
	var TaskList []*pb.ModelTask
	self.ModelInfo.GetList(uid, &TaskList)

	return TaskList
}

// GetListByTaskType 获取玩家某一类型的任务
func (self *Task) GetListByTaskType(uid string, taskType int32) []*pb.ModelTask {
	var taskList []*pb.ModelTask
	_chkList := self.GetList(uid)
	for _, task := range _chkList {
		if task.Type != taskType {
			continue
		}
		taskList = append(taskList, task)
	}

	return taskList
}

// GetUnFinishByStype 获取玩家stype类型的未完成任务
func (self *Task) GetUnFinishByStype(uid string, taskStype int32) []*pb.ModelTask {
	var taskList []*pb.ModelTask
	_chkList := self.GetList(uid)
	for _, task := range _chkList {
		// stype不对
		if task.SType != taskStype {
			continue
		}
		// 已完成
		if task.Finish == 1 {
			continue
		}
		taskList = append(taskList, task)
	}

	return taskList
}

// GetInfo 获取单个任务数据
func (self *Task) GetInfo(oid string) *pb.ModelTask {
	TaskInfo := new(pb.ModelTask)
	self.Get(oid, TaskInfo)

	return TaskInfo
}
