package main

import (
	"gogame/game/module"
	"gogame/gameservice"
	_ "gogame/gameservice/timer"
)

func main() {
	// 定时器注册
	module.InitRoute()

	// 启动timer
	gameservice.RunTimer()

	// 等待服务结束
	gameservice.WaitStop()
}
