package modellog

import "go.mongodb.org/mongo-driver/bson"

// BsonDToBsonM bson.D转bson.M
func BsonDToBsonM(bsonD any) bson.M {
	bsonM := bson.M{}
	for _, bsonE := range bsonD.(bson.D) {
		bsonM[bsonE.Key] = bsonE.Value
	}

	return bsonM
}

// BsonDToMap bson.D转map
func BsonDToMap(bsonD any) map[string]any {
	_map := make(map[string]any)
	for _, bsonE := range bsonD.(bson.D) {
		_map[bsonE.Key] = bsonE.Value
	}

	return _map
}

// BsonDListToMapList bson.D List 转 map List
func BsonDListToMapList(bsonDList []any) []any {
	var _mapList []any
	for _, bsonD := range bsonDList {
		_map := make(map[string]any)
		for _, bsonE := range bsonD.(bson.D) {
			_map[bsonE.Key] = bsonE.Value
		}
		_mapList = append(_mapList, _map)
	}

	return _mapList
}
