package go_rpc

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gogame/server/network/tcp"
)

type RpcStruct struct {
	Type      string      // rpc类型
	Name      string      // rpc名字
	Address   string      // rpcClient链接的地址/rpcServer监听地址
	Service   *rpcService // 服务
	rpcHandle *rpcHandle  // 数据处理
}

func (r *RpcStruct) GetType() string {
	return r.Type
}

func (r *RpcStruct) GetName() string {
	return r.Name
}

func (r *RpcStruct) GetAddress() string {
	return r.Address

}

func (r *RpcStruct) Write(msg []byte) {
	err := r.rpcHandle.Write(msg)
	if err != nil {
		log.Warning("RpcStruct Write error!! -->", err)
	}
}

func (r *RpcStruct) Handle(conn tcp.ConnChannelInterface) {
	r.rpcHandle.Run(context.Background(), conn)
}
