package go_rpc

import (
	"sync"
)

const MagicNumber uint8 = 0x0f

const (
	MagicNumberLength = 1                                                            // 包标识长度
	MsgIdLength       = 4                                                            // 消息id长度
	MsgTypeLength     = 1                                                            // 包类型长度
	DataLength        = 8                                                            // 包体长度
	PackHeadLength    = MagicNumberLength + MsgIdLength + MsgTypeLength + DataLength // 包头长度
)

type Message struct {
	msgId   uint32 // 消息id
	msgType uint8  // 消息内容
	msg     []byte // 消息
}

var pool = sync.Pool{New: func() interface{} {
	return &Message{}
}}

func GetMessage() *Message {
	return pool.Get().(*Message)
}

func PutMessage(msg *Message) {
	pool.Put(msg)
}
