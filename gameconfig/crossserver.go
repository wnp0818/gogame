package gameconfig

type CrossServerConfig struct {
	Redis   Redis   `json:"redis"`
	MongoDb MongoDb `json:"mongodb"` // mongodb配置

}

var CrossServer = LoadCrossServerConfig()
