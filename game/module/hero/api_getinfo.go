package hero

import (
	g "gogame/game"
	"gogame/pb"
	"gogame/server/rpcx"
)

func (h *Api) GetInfoCheck(args *pb.HeroGetInfoArgs, res *rpcx.Response) (_heroInfo *pb.ModelHero) {
	res.S = 1
	res.IsCheck = 1

	_heroInfo = g.DATA.Hero.GetInfo(args.Oid)
	// 没有这个英雄
	if _heroInfo.Uid == "" {
		res.S = -1
		res.ErrorMsg = g.L("hero_error_1")
		return
	}

	return

}

// GetInfo 获取英雄数据
func (h *Api) GetInfo(request *rpcx.Request, args *pb.HeroGetInfoArgs, res *rpcx.Response) {
	_heroInfo := h.GetInfoCheck(args, res)

	res.SendMsg(&pb.HeroGetInfoRes{
		HeroInfo: _heroInfo,
	})
}
