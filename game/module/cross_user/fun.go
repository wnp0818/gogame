package cross_user

import (
	"gogame/game/manage/module"
	"gogame/game/module/base"
	"gogame/gameservice/worker"
)

type ModuleCrossUser struct {
	*base.ModuleBase
	Api
}

var CrossUser *ModuleCrossUser

func init() {
	CrossUser = &ModuleCrossUser{
		ModuleBase: base.NewModuleBase(),
	}

	_moduleName := "cross_user"
	// 路由注册 未定义Api2Func默认采用反射筛选符合条件的方法注册和调用
	worker.Register(_moduleName, CrossUser)
	// 模块注册
	module.Manage.CrossUser = CrossUser

}
