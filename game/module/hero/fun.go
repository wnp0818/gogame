package hero

import (
	g "gogame/game"
	"gogame/game/manage/module"
	"gogame/game/module/base"
	"gogame/gameservice/worker"
	"gogame/pb"
)

type ModuleHero struct {
	*base.ModuleBase
	Api
}

var Hero *ModuleHero

func init() {
	Hero = &ModuleHero{
		ModuleBase: base.NewModuleBase(),
	}

	_moduleName := "hero"
	// 路由注册
	worker.Register(_moduleName, Hero)
	// 模块注册
	module.Manage.Hero = Hero
}

// GetDefHeroInfo 获取初始英雄数据
func (m *ModuleHero) GetDefHeroInfo(uid string, hid string) *pb.ModelHero {
	_heroCon := g.GC.Hero().Get(hid)
	// 英雄配置不存在
	if _heroCon == nil {
		return nil
	}

	_heroInfo := &pb.ModelHero{
		//Id:    g.C.GetObjectId(),
		Uid:   uid,
		Hid:   hid,
		Star:  _heroCon.Star,
		Lv:    1,
		Ctime: g.C.Now(),
	}

	return _heroInfo
}
