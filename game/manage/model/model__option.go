package model

import (
	"time"
)

type OptionFunc func(m *ModelInfo)

// SetExpireTime 设置数据过期时间
func SetExpireTime(expireTime time.Duration) OptionFunc {
	return func(m *ModelInfo) {
		m.ExpireTime = int64(expireTime.Seconds())
	}
}

// WriteRedis 数据是否写入redis 用于gamelog这种只插入不查询的表
func WriteRedis(flag bool) OptionFunc {
	return func(m *ModelInfo) {
		m.WriteRedis = flag
	}
}
