package game

import (
	"fmt"
)

// fmtUid2GatewayUrl 格式化玩家唯一gateway key
func fmtUid2GatewayUrl(uid string) string {
	return fmt.Sprintf("%s_gateway", uid)
}

// GetUidGatewayUrl 获取玩家当前所处gateway地址
func GetUidGatewayUrl(uid string) string {
	return Redis.Get(fmtUid2GatewayUrl(uid)).Val()
}

// SetUidGatewayUrl 设置玩家当前所处gateway地址
func SetUidGatewayUrl(uid, gatewayUrl string) {
	Redis.Set(fmtUid2GatewayUrl(uid), gatewayUrl, 0)

}

// DelUidGatewayUrl 清除玩家当前所处gateway地址
func DelUidGatewayUrl(uid string) {
	Redis.Del(fmtUid2GatewayUrl(uid))
}

// NoticeSaveUserData 通知保存玩家数据
func NoticeSaveUserData(uid string) {
	RpcDo(
		nil,
		map[string]string{"uid": uid},
		"NoticeSaveUserData",
		"modelLog:-1",
	)
}
