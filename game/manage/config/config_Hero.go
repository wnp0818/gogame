package config

import cfg "gogame/game/manage/config/structs"

type Hero struct {
	StructHandle
	*cfg.GameHero
	NewFunc func(_buf []map[string]any) (*cfg.GameHero, error) // 数据生成方法

	// 预处理字段定义区域---start

	// 预处理字段定义区域---end
}

func init() {
	config := &Hero{
		StructHandle: NewStructHandle("game_Hero"),
		NewFunc:      cfg.NewGameHero,
	}
	Manage.configs.Hero = config
	RegisterConfigInterface(config.Name, config)
}

// Load 加载配置
func (self *Hero) Load(jsonData any) error {
	data, err := self.NewFunc(jsonData.([]map[string]any))
	if err != nil {
		return err
	}
	self.GameHero = data
	self.PreConfig()
	return nil
}

// PreConfig 配置预处理
func (self *Hero) PreConfig() {

}

// ------------------------ 以上文件内容自动生成,请勿随便修改! ------------------------
// ------------------------ 下方、定义配置diy方法 ------------------------

// GetListByStar 获取x星以上的英雄
func (self *Hero) GetListByStar(star int32) []*cfg.GameHeroData {
	_heroList := make([]*cfg.GameHeroData, 0)
	for _, h := range self.GetDataList() {
		// 星级不足
		if h.Star < star {
			continue
		}
		_heroList = append(_heroList, h)
	}
	return _heroList
}
