package main

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"go.mongodb.org/mongo-driver/bson"
	g "gogame/game"
	cfg "gogame/game/manage/config/structs"
	"gogame/game/module"
	"gogame/gameconfig"
	"gogame/gameservice"
	"gogame/lib"
	"gogame/pb"
	"runtime"
	"time"
)

func init() {
	module.InitRoute()
	//evalShaStr = g.Redis.Client.ScriptLoad(context.Background(), luaScript).Val()
	//fmt.Println(fmt.Sprintf("sha-->%s", evalShaStr))
}

var evalShaStr string

var luaScript = `
local key    = tostring(KEYS[1])
local keyss = redis.call("HGETALL", key)
local data = {}
local n = 1
for i, v in ipairs(keyss) do
	if i%2 == 0 then
		data[n] = redis.call("HGETALL", v)
		n = n+1
	end
end 
return data
`

//var redisHashTag = ""

var redisHashTag = "{herotest}"

func addData(uid, heroRedisKey string, addNum int) {
	g.Mdb.Delete("hero", bson.M{"uid": uid})
	var _heroList []any
	for i := 0; i <= addNum; i++ {
		_heroList = append(_heroList, g.M.Hero.GetDefHeroInfo(uid, "11001"))
	}
	g.Mdb.InsertMany("hero", _heroList)
	_setData := map[string]string{}
	for _, h := range _heroList {
		h := h.(*pb.ModelHero)
		key, val := h.Id, fmt.Sprintf("hero_%s_%s%s", uid, h.Id, redisHashTag)
		_setData[key] = val
		g.Redis.Client.HMSet(context.Background(), val, lib.Convert.Struct2Map(h))
	}

	g.Redis.Client.HMSet(context.Background(), heroRedisKey, _setData)
}

func delData(uid, heroRedisKey string) {
	res := g.Redis.Client.HGetAll(context.Background(), heroRedisKey).Val()
	keys := make([]string, 0)
	vals := make([]string, 0)
	for key, val := range res {
		keys = append(keys, key)
		vals = append(vals, val)
	}
	g.Redis.Client.HDel(context.Background(), heroRedisKey, keys...)

	for _, vk := range vals {
		res2 := g.Redis.Client.HGetAll(context.Background(), vk).Val()
		_delKs := make([]string, 0)
		for kk, _ := range res2 {
			_delKs = append(_delKs, kk)
		}
		g.Redis.Client.HDel(context.Background(), vk, _delKs...)

	}

	//res3 := g.Redis.Client.HGetAll(context.Background(), heroRedisKey).Val()
	//fmt.Println(res3)

}

func RedisFunc(heroRedisKey string) {
	_sTime := lib.Time.NowMillisecond()
	// redis获取当前玩家所有英雄keys
	res := g.Redis.Client.HGetAll(context.Background(), heroRedisKey).Val()
	var _heroListRedis []*pb.ModelHero
	// 遍历keys通过redis查出每个英雄的数据
	for _, v := range res {
		val := g.Redis.Client.HGetAll(context.Background(), v).Val()
		_heroInfo := &pb.ModelHero{}
		lib.Convert.Map2Struct(val, &_heroInfo)
		_heroListRedis = append(_heroListRedis, _heroInfo)
	}
	_diffTime := lib.Time.NowMillisecond() - _sTime
	fmt.Printf("\n          redis耗时：           %dms  数据长度：%d   ", _diffTime, len(_heroListRedis))
}

func RedisFuncPipeline(heroRedisKey string, num int) {
	var _sumTime int64
	for i := 0; i <= num; i++ {
		_sTime := lib.Time.NowMillisecond()

		// redis获取当前玩家所有英雄keys
		res := g.Redis.Client.HGetAll(context.Background(), heroRedisKey).Val()
		_pipeline := g.Redis.Client.Pipeline()
		// 遍历keys通过redis查出每个英雄的数据
		for _, v := range res {
			_pipeline.HGetAll(context.Background(), v)
		}
		_execRes, _ := _pipeline.Exec(context.Background())

		var _heroListRedis []*pb.ModelHero
		for _, vv := range _execRes {
			val := vv.(*redis.StringStringMapCmd).Val()
			_heroInfo := &pb.ModelHero{}
			lib.Convert.Map2Struct(val, &_heroInfo)
			_heroListRedis = append(_heroListRedis, _heroInfo)
		}

		_diffTime := lib.Time.NowMillisecond() - _sTime
		_sumTime = _sumTime + _diffTime
	}

	_resTime := _sumTime / int64(num)
	fmt.Printf("\n          redis-pipeline耗时：  %dms", _resTime)

}

func RedisFuncEvalSha(heroRedisKey string, num int) {
	var _sumTime int64
	for i := 0; i <= num; i++ {
		_sTime := lib.Time.NowMillisecond()
		var _heroListRedis []*pb.ModelHero
		res := g.Redis.Client.EvalSha(context.Background(), evalShaStr, []string{heroRedisKey})
		_data := res.Val().([]any)
		// 遍历转换为英雄数据
		for _, v := range _data {
			_mapData := make(map[string]string)
			_tmpV := v.([]any)
			for i := 0; i < len(_tmpV); i += 2 {
				_mapData[_tmpV[i].(string)] = _tmpV[i+1].(string)
			}

			_heroInfo := &pb.ModelHero{}
			lib.Convert.Map2Struct(_mapData, &_heroInfo)
			_heroListRedis = append(_heroListRedis, _heroInfo)
		}
		// redis获取当前玩家所有英雄keys
		_diffTime := lib.Time.NowMillisecond() - _sTime
		_sumTime = _sumTime + _diffTime
	}
	_resTime := _sumTime / int64(num)
	fmt.Printf("          redis-EvalSha耗时：   %dms", _resTime)

}

func MongoDbFunc(uid string, num int) {
	var _sumTime int64
	for i := 0; i <= num; i++ {
		_sTime := lib.Time.NowMillisecond()
		var _heroListMongo []*pb.ModelHero
		// 一次从mongo取出所有英雄数据
		g.Mdb.FindByReflect("hero", bson.M{"uid": uid}, &_heroListMongo)
		_diffTime := lib.Time.NowMillisecond() - _sTime
		_sumTime = _sumTime + _diffTime
	}

	_resTime := _sumTime / int64(num)
	fmt.Printf("\n          mongo耗时：           %dms\n", _resTime)

}

func RedisMongoTest(uid string) {
	heroRedisKey := fmt.Sprintf("model_hero_%s%s", uid, redisHashTag)

	delData(uid, heroRedisKey)

	_testNum := []int{100}
	for _, testNum := range _testNum {
		_numList := []int{100, 1000, 10000}
		for _, addNum := range _numList {
			delData(uid, heroRedisKey)
			addData(uid, heroRedisKey, addNum)
			fmt.Println(fmt.Sprintf("%d条数据测试%d次,平均耗时：", addNum, testNum))

			/* --------- redis取英雄数据 --------- */
			RedisFuncEvalSha(heroRedisKey, testNum)
			RedisFuncPipeline(heroRedisKey, testNum)
			//RedisFunc(heroRedisKey)

			/* --------- mongo取英雄数据 --------- */
			MongoDbFunc(uid, testNum)
			fmt.Println("----------------------------------------------------------------------------")
		}
		fmt.Println()
	}

}

const uid = "0_63282819bedb1c3d6249834c"
const id = "62d8f027a49d94a819cf69af"

func test_main() {
	// 接口注册
	module.InitRoute()

	// 启动worker
	gameservice.RunWorker()

	time.Sleep(time.Second * 2)

	// 启动定时器
	gameservice.RunTimer()
	// 启动model_log
	gameservice.RunModelLog()

	_gameVer := gameconfig.ServerConfig.GetGameVer()
	switch _gameVer {
	case "cross":
		g.CrossRedis.FlushAll()
	default:
		g.Redis.FlushAll()

		// 启动gateway
		gameservice.RunGateway()
	}

	// 等待所有服务结束
	gameservice.WaitStop()
}

func getPrize() {
	_prize := []*cfg.Gameatn{
		//{
		//	A: "attr",
		//	T: "lv",
		//	N: 100,
		//},
		//{
		//	A: "attr",
		//	T: "jinbi",
		//	N: 100000,
		//},
		//{
		//	A: "hero",
		//	T: "13001",
		//	N: 2,
		//},
		//{
		//	A: "hero",
		//	T: "13001",
		//	N: 3,
		//},
		{
			A: "item",
			T: "10001",
			N: 3,
		},
		{
			A: "item",
			T: "10002",
			N: 3,
		},
	}
	g.GetPrizeRes(uid, _prize, nil, "nil", true)
}

func delNeed() {
	_need := []*cfg.Gameatn{
		{A: "attr", T: "lv", N: 7},
		{A: "item", T: "10001", N: 3},
		{A: "item", T: "10001", N: 4},
		{A: "item", T: "10002", N: 2},
	}
	g.DelNeed(uid, _need, nil, "", false)
}

func main() {
	//RedisMongoTest(uid)
	g.Redis.FlushAll()

	//test_main()

	fmt.Println(g.C.TTLTime())

	for {
		runtime.Gosched()
	}
}
