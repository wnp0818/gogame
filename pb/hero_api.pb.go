// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.0
// 	protoc        v3.21.1
// source: hero/hero_api.proto

package pb

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// 获取英雄列表
type HeroGetListArgs struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *HeroGetListArgs) Reset() {
	*x = HeroGetListArgs{}
	if protoimpl.UnsafeEnabled {
		mi := &file_hero_hero_api_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *HeroGetListArgs) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*HeroGetListArgs) ProtoMessage() {}

func (x *HeroGetListArgs) ProtoReflect() protoreflect.Message {
	mi := &file_hero_hero_api_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use HeroGetListArgs.ProtoReflect.Descriptor instead.
func (*HeroGetListArgs) Descriptor() ([]byte, []int) {
	return file_hero_hero_api_proto_rawDescGZIP(), []int{0}
}

type HeroGetListRes struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	HeroList []*ModelHero `protobuf:"bytes,1,rep,name=HeroList,proto3" json:"herolist" redis:"herolist" bson:"herolist"`
}

func (x *HeroGetListRes) Reset() {
	*x = HeroGetListRes{}
	if protoimpl.UnsafeEnabled {
		mi := &file_hero_hero_api_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *HeroGetListRes) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*HeroGetListRes) ProtoMessage() {}

func (x *HeroGetListRes) ProtoReflect() protoreflect.Message {
	mi := &file_hero_hero_api_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use HeroGetListRes.ProtoReflect.Descriptor instead.
func (*HeroGetListRes) Descriptor() ([]byte, []int) {
	return file_hero_hero_api_proto_rawDescGZIP(), []int{1}
}

func (x *HeroGetListRes) GetHeroList() []*ModelHero {
	if x != nil {
		return x.HeroList
	}
	return nil
}

// 获取英雄数据
type HeroGetInfoArgs struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Oid string `protobuf:"bytes,1,opt,name=Oid,proto3" json:"oid" redis:"oid" bson:"oid"`
}

func (x *HeroGetInfoArgs) Reset() {
	*x = HeroGetInfoArgs{}
	if protoimpl.UnsafeEnabled {
		mi := &file_hero_hero_api_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *HeroGetInfoArgs) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*HeroGetInfoArgs) ProtoMessage() {}

func (x *HeroGetInfoArgs) ProtoReflect() protoreflect.Message {
	mi := &file_hero_hero_api_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use HeroGetInfoArgs.ProtoReflect.Descriptor instead.
func (*HeroGetInfoArgs) Descriptor() ([]byte, []int) {
	return file_hero_hero_api_proto_rawDescGZIP(), []int{2}
}

func (x *HeroGetInfoArgs) GetOid() string {
	if x != nil {
		return x.Oid
	}
	return ""
}

type HeroGetInfoRes struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	HeroInfo *ModelHero `protobuf:"bytes,1,opt,name=heroInfo,proto3" json:"heroinfo" redis:"heroinfo" bson:"heroinfo"`
}

func (x *HeroGetInfoRes) Reset() {
	*x = HeroGetInfoRes{}
	if protoimpl.UnsafeEnabled {
		mi := &file_hero_hero_api_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *HeroGetInfoRes) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*HeroGetInfoRes) ProtoMessage() {}

func (x *HeroGetInfoRes) ProtoReflect() protoreflect.Message {
	mi := &file_hero_hero_api_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use HeroGetInfoRes.ProtoReflect.Descriptor instead.
func (*HeroGetInfoRes) Descriptor() ([]byte, []int) {
	return file_hero_hero_api_proto_rawDescGZIP(), []int{3}
}

func (x *HeroGetInfoRes) GetHeroInfo() *ModelHero {
	if x != nil {
		return x.HeroInfo
	}
	return nil
}

var File_hero_hero_api_proto protoreflect.FileDescriptor

var file_hero_hero_api_proto_rawDesc = []byte{
	0x0a, 0x13, 0x68, 0x65, 0x72, 0x6f, 0x2f, 0x68, 0x65, 0x72, 0x6f, 0x5f, 0x61, 0x70, 0x69, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x15, 0x68, 0x65, 0x72, 0x6f, 0x2f, 0x68, 0x65, 0x72, 0x6f,
	0x5f, 0x6d, 0x6f, 0x6e, 0x67, 0x6f, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x11, 0x0a, 0x0f,
	0x48, 0x65, 0x72, 0x6f, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x41, 0x72, 0x67, 0x73, 0x22,
	0x38, 0x0a, 0x0e, 0x48, 0x65, 0x72, 0x6f, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65,
	0x73, 0x12, 0x26, 0x0a, 0x08, 0x48, 0x65, 0x72, 0x6f, 0x4c, 0x69, 0x73, 0x74, 0x18, 0x01, 0x20,
	0x03, 0x28, 0x0b, 0x32, 0x0a, 0x2e, 0x4d, 0x6f, 0x64, 0x65, 0x6c, 0x48, 0x65, 0x72, 0x6f, 0x52,
	0x08, 0x48, 0x65, 0x72, 0x6f, 0x4c, 0x69, 0x73, 0x74, 0x22, 0x23, 0x0a, 0x0f, 0x48, 0x65, 0x72,
	0x6f, 0x47, 0x65, 0x74, 0x49, 0x6e, 0x66, 0x6f, 0x41, 0x72, 0x67, 0x73, 0x12, 0x10, 0x0a, 0x03,
	0x4f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x4f, 0x69, 0x64, 0x22, 0x38,
	0x0a, 0x0e, 0x48, 0x65, 0x72, 0x6f, 0x47, 0x65, 0x74, 0x49, 0x6e, 0x66, 0x6f, 0x52, 0x65, 0x73,
	0x12, 0x26, 0x0a, 0x08, 0x68, 0x65, 0x72, 0x6f, 0x49, 0x6e, 0x66, 0x6f, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x0a, 0x2e, 0x4d, 0x6f, 0x64, 0x65, 0x6c, 0x48, 0x65, 0x72, 0x6f, 0x52, 0x08,
	0x68, 0x65, 0x72, 0x6f, 0x49, 0x6e, 0x66, 0x6f, 0x42, 0x06, 0x5a, 0x04, 0x2e, 0x3b, 0x70, 0x62,
	0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_hero_hero_api_proto_rawDescOnce sync.Once
	file_hero_hero_api_proto_rawDescData = file_hero_hero_api_proto_rawDesc
)

func file_hero_hero_api_proto_rawDescGZIP() []byte {
	file_hero_hero_api_proto_rawDescOnce.Do(func() {
		file_hero_hero_api_proto_rawDescData = protoimpl.X.CompressGZIP(file_hero_hero_api_proto_rawDescData)
	})
	return file_hero_hero_api_proto_rawDescData
}

var file_hero_hero_api_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_hero_hero_api_proto_goTypes = []interface{}{
	(*HeroGetListArgs)(nil), // 0: HeroGetListArgs
	(*HeroGetListRes)(nil),  // 1: HeroGetListRes
	(*HeroGetInfoArgs)(nil), // 2: HeroGetInfoArgs
	(*HeroGetInfoRes)(nil),  // 3: HeroGetInfoRes
	(*ModelHero)(nil),       // 4: ModelHero
}
var file_hero_hero_api_proto_depIdxs = []int32{
	4, // 0: HeroGetListRes.HeroList:type_name -> ModelHero
	4, // 1: HeroGetInfoRes.heroInfo:type_name -> ModelHero
	2, // [2:2] is the sub-list for method output_type
	2, // [2:2] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_hero_hero_api_proto_init() }
func file_hero_hero_api_proto_init() {
	if File_hero_hero_api_proto != nil {
		return
	}
	file_hero_hero_mongo_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_hero_hero_api_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*HeroGetListArgs); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_hero_hero_api_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*HeroGetListRes); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_hero_hero_api_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*HeroGetInfoArgs); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_hero_hero_api_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*HeroGetInfoRes); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_hero_hero_api_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_hero_hero_api_proto_goTypes,
		DependencyIndexes: file_hero_hero_api_proto_depIdxs,
		MessageInfos:      file_hero_hero_api_proto_msgTypes,
	}.Build()
	File_hero_hero_api_proto = out.File
	file_hero_hero_api_proto_rawDesc = nil
	file_hero_hero_api_proto_goTypes = nil
	file_hero_hero_api_proto_depIdxs = nil
}
