package rpcx

type RpcXStruct struct {
	BasePath    string // 根路径
	Type        string // rpc类型
	Name        string // rpc名字
	ServiceName string // 服务名
	Address     string // rpcClient链接的地址/rpcServer监听地址
}

func (r *RpcXStruct) GetType() string {
	return r.Type
}

func (r *RpcXStruct) GetName() string {
	return r.Name
}

func (r *RpcXStruct) GetAddress() string {
	return r.Address

}
