package pb

import "sync"

/*
 ---------- pool request ----------
*/
type requestPool struct {
	pool *sync.Pool
}

var RequestPool = &requestPool{
	pool: &sync.Pool{
		New: func() any {
			return &Request{}
		},
	},
}

func (p *requestPool) Get() *Request {
	return p.pool.Get().(*Request)
}

func (p *requestPool) Put(request *Request) {
	request.ModuleName = ""
	request.ApiName = ""
	request.Data = nil
	request.Sec = ""

	p.pool.Put(request)
}

func NewRequest() *Request {
	return RequestPool.Get()
}

// PutRequest 资源回收
func PutRequest(request *Request) {
	RequestPool.Put(request)
}

/*
 ---------- pool request ---------- end
*/

/*
 ---------- pool ApiRequest ----------
*/
type apiRequestPool struct {
	pool *sync.Pool
}

var ApiRequestPool = &apiRequestPool{
	pool: &sync.Pool{
		New: func() any {
			return &ApiRequest{}
		},
	},
}

func (p *apiRequestPool) Get() *ApiRequest {
	return p.pool.Get().(*ApiRequest)
}

func (p *apiRequestPool) Put(apiRequest *ApiRequest) {
	apiRequest.ModuleName = ""
	apiRequest.ApiName = ""
	apiRequest.Data = nil

	apiRequest.Ip = ""
	apiRequest.Uid = ""
	apiRequest.GatewayUrl = ""
	apiRequest.WorkerUrl = ""

	p.pool.Put(apiRequest)
}

func NewApiRequest() *ApiRequest {
	return ApiRequestPool.Get()
}

// PutApiRequest 资源回收
func PutApiRequest(apiRequest *ApiRequest) {
	ApiRequestPool.Put(apiRequest)
}

/*
 ---------- pool ApiRequest ---------- end
*/
