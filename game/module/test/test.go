package test

import (
	"gogame/pb"
	"gogame/server/rpcx"
)

var (
	Request = &rpcx.Request{
		ApiRequest: &pb.ApiRequest{},
	}
	Response = &rpcx.Response{}
)
