package ws

import (
	"github.com/gorilla/websocket"
)

// NewClient 创建一个ws客户端
func NewClient(address string) *websocket.Conn {
	_client, _, _err := websocket.DefaultDialer.Dial(address, nil)
	if _err != nil {
		return nil
	}

	return _client
}
