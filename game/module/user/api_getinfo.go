package user

import (
	"fmt"
	g "gogame/game"
	"gogame/pb"
	"gogame/server/rpcx"
)

func (h *Api) GetInfoCheck(args *pb.UserGetInfoArgs, res *rpcx.Response) *pb.ModelUser {
	res.IsCheck = 1
	res.S = 1

	_userInfo := g.GetGud(args.Uid)
	// 玩家不存在
	if _userInfo.Uid == "" {
		res.S = -1
		res.ErrorMsg = g.L("user_error_3")
		return _userInfo
	}

	return _userInfo

}

// GetInfo 获取玩家数据
func (h *Api) GetInfo(request *rpcx.Request, args any, res *rpcx.Response) {
	_args := args.(*pb.UserGetInfoArgs)
	_userInfo := h.GetInfoCheck(_args, res)
	// 条件检测不通过
	if res.S != 1 {
		return
	}

	//// 推送到指定玩家
	//g.SendMsgByUid(request.Uid, fmt.Sprintf("hello~~my name is %s", request.Uid))
	// 推送到所有人
	g.SendMsgByAll(fmt.Sprintf("hi~~ my name is %s", g.GetGud(request.Uid).Name))

	res.SendMsg(&pb.UserGetInfoRes{
		UserInfo: _userInfo,
	})

}
