package base

import "gogame/gameservice/worker"

type ModuleBase struct {
	Api2Func worker.Api2FuncInfo
}

func NewModuleBase() *ModuleBase {
	return &ModuleBase{
		Api2Func: nil,
	}
}

// GetApi2Func 获取当前模块所有接口
func (m *ModuleBase) GetApi2Func() worker.Api2FuncInfo {
	if m.Api2Func == nil {
		return nil
	}
	return m.Api2Func
}
