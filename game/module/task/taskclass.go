package task

import (
	g "gogame/game"
	cfg "gogame/game/manage/config/structs"
)

/*
    所有任务类在这里定义
	每个任务stype对应一个任务类 类名 = taskoption + 任务stype
	例如：
		累计登陆x次游戏(每日算1次)的任务stype是1
		那么需定义任务类-->参考task1

	任务事件监听统一在model层的DataChange中监听,统一管理
	参考model_userinfo的DataChange

*/

func init() {

}

// Task1 累计登陆x次游戏(每日算1次)
var task1 = NewTask(&BaseTask{
	EventName: "task1",
	IsInc:     true,

	FuncInitVal: func(uid string, taskCon *cfg.GameTaskData) int32 {
		return int32(g.GetGud(uid).LoginNum)
	},
})

// Task2 进行x次玩家等级提升
var task2 = NewTask(&BaseTask{
	EventName: "task2",
	IsInc:     true,
})
