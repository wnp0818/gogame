package rpcx

import (
	"fmt"
	"gogame/gameconfig"
)

const (
	CrossApi        = "cross_api"
	Api             = "api"
	DefaultBasePath = "/worker"
)

// GetDefaultServiceName 默认rpcX服务
func GetDefaultServiceName() string {
	_gameVer := gameconfig.ServerConfig.GetGameVer()
	var defaultServiceName string
	switch _gameVer {
	case "cross":
		defaultServiceName = CrossApi
	default:
		defaultServiceName = Api
	}

	return defaultServiceName
}

// GetCrossApiBasePath 获取跨服worker的basePath
func GetCrossApiBasePath() string {
	return fmt.Sprintf("/crossver_%s_worker", gameconfig.ServerConfig.CrossVer)
}

// GetApiBasePath 获取本服worker的basePath
func GetApiBasePath() string {
	return fmt.Sprintf("/%d_worker", gameconfig.ServerConfig.Sid)
}

// GetClientBasePath client rpcx服务发现path
func GetClientBasePath(serviceName string) string {
	var basePath string
	switch serviceName {
	case CrossApi:
		basePath = GetCrossApiBasePath()
	case Api:
		basePath = GetApiBasePath()
	default:
		basePath = DefaultBasePath
	}

	return basePath
}

// GetServerBasePath server rpcx服务发现path
func GetServerBasePath() string {
	var basePath string
	switch gameconfig.ServerConfig.Ver {
	case "cross":
		basePath = GetCrossApiBasePath()
	default:
		basePath = GetApiBasePath()
	}

	return basePath
}
