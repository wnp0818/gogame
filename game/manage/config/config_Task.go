package config

import (
	cfg "gogame/game/manage/config/structs"
)

type Task struct {
	StructHandle
	*cfg.GameTask
	NewFunc func(_buf []map[string]any) (*cfg.GameTask, error) // 数据生成方法

	// 预处理字段定义区域---start
	InitTaskList        []*cfg.GameTaskData           // 玩家初始任务
	Lv2TaskList         map[int32][]*cfg.GameTaskData // 等级对应的任务
	TaskId2NextId       map[int32]int32               // 任务id对应的下一个任务
	TaskId2PVal         map[int32]int32               // 任务id对应的任务所需值
	TaskType2TaskIdList map[int32][]int32             // 任务类型对应的任务id列表
	// 预处理字段定义区域---end
}

func init() {
	config := &Task{
		StructHandle: NewStructHandle("game_Task"),
		NewFunc:      cfg.NewGameTask,

		InitTaskList:        make([]*cfg.GameTaskData, 0),
		Lv2TaskList:         make(map[int32][]*cfg.GameTaskData),
		TaskId2NextId:       make(map[int32]int32),
		TaskId2PVal:         make(map[int32]int32),
		TaskType2TaskIdList: make(map[int32][]int32),
	}
	Manage.configs.Task = config
	RegisterConfigInterface(config.Name, config)
}

// Load 加载配置
func (self *Task) Load(jsonData any) error {
	data, err := self.NewFunc(jsonData.([]map[string]any))
	if err != nil {
		return err
	}
	self.GameTask = data
	self.PreConfig()
	return nil
}

// PreConfig 配置预处理
func (self *Task) PreConfig() {
	for _, task := range self.GetDataList() {
		_taskId, _taskType := task.Taskid, task.Type
		self.TaskId2PVal[_taskId] = task.Pval

		// 任务类型分类
		if self.TaskType2TaskIdList[_taskType] == nil {
			self.TaskType2TaskIdList[_taskType] = make([]int32, 0)
		}
		self.TaskType2TaskIdList[_taskType] = append(self.TaskType2TaskIdList[_taskType], _taskId)

		_fTaskId := task.Qianzhi
		// 存在前置任务
		if _fTaskId > 0 {
			self.TaskId2NextId[_fTaskId] = _taskId
		} else {
			// 任务等级分类
			_openLv := task.Openlv
			if self.Lv2TaskList[_openLv] == nil {
				self.Lv2TaskList[_openLv] = make([]*cfg.GameTaskData, 0)
			}
			self.Lv2TaskList[_openLv] = append(self.Lv2TaskList[_openLv], task)

			// 1级且没有前置的的任务都是初始任务
			if _openLv == 1 {
				self.InitTaskList = append(
					self.InitTaskList,
					task,
				)
			}
		}
	}

}

// ------------------------ 以上文件内容自动生成,请勿随便修改! ------------------------
// ------------------------ 下方、定义配置diy方法 ------------------------

// HasType 是否存在这个任务类型
func (self *Task) HasType(taskType int32) bool {
	_, ok := self.TaskType2TaskIdList[taskType]
	return ok
}
