package hero

import (
	g "gogame/game"
	"gogame/pb"
	"gogame/server/rpcx"
)

func (h *Api) GetListCheck(res *rpcx.Response) {
	res.S = 1
	res.IsCheck = 1
}

// GetList 获取玩家所有英雄
func (h *Api) GetList(request *rpcx.Request, args *pb.HeroGetListArgs, res *rpcx.Response) {
	h.GetListCheck(res)

	res.SendMsg(&pb.HeroGetListRes{
		HeroList: g.DATA.Hero.GetList(request.Uid),
	})
}
