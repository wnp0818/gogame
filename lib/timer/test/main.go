package main

import (
	"gogame/game/module"
	"gogame/lib/timer"
)

func main() {
	module.InitRoute()
	_timer := timer.NewTimer()
	_timer.Run("timer")

	select {}
}
