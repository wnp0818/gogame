package config

import (
	"fmt"
	jsoniter "github.com/json-iterator/go"
)

/*
json格式处理
*/

type JsonHandle struct {
	JsonData map[string]any // json数据格式
	Name     string         // json名
}

func NewJsonHandle(name string) *JsonHandle {
	return &JsonHandle{
		JsonData: make(map[string]any),
		Name:     name,
	}
}

// UnMarshal 解序列文件内容
func (s *JsonHandle) UnMarshal(fileData []byte) (any, error) {
	var (
		_data map[string]any
		err   error
	)
	// 转换成map[string]any
	err = jsoniter.Unmarshal(fileData, &_data)

	return _data, err
}

// SetLoad 设置已加载
func (s *JsonHandle) SetLoad() {
	SetLoad(s.Name)
}

// Load 加载配置
func (s *JsonHandle) Load(jsonData any) error {
	data, ok := jsonData.(map[string]any)
	if !ok {
		return fmt.Errorf("文件内容格式不对")
	}
	s.JsonData = data

	return nil
}

/*
struct格式处理
*/

type StructHandle struct {
	Name string // json名
}

func NewStructHandle(name string) StructHandle {
	return StructHandle{
		Name: name,
	}
}

// UnMarshal 解序列文件内容
func (s StructHandle) UnMarshal(fileData []byte) (any, error) {
	var (
		_dataList []map[string]any
		err       error
	)
	// 转换成[]map[string]any
	err = jsoniter.Unmarshal(fileData, &_dataList)

	return _dataList, err
}

// SetLoad 设置已加载
func (s StructHandle) SetLoad() {
	SetLoad(s.Name)
}
