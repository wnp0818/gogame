package user

import (
	g "gogame/game"
	"gogame/game/module/hero"
	"testing"
	"time"
)

func Test_Run(t *testing.T) {
	g.Redis.FlushAll()
	ApiLogin()
	ApiChangeName()
	hero.ApiGetList()
	time.Sleep(time.Second * 5)
}
