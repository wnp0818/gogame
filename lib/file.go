package lib

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strings"
)

var RootPath string

func init() {
	RootPath = getRootPath()
}

// GetFilePath 获取文件路径
func GetFilePath(addPath string) string {
	return RootPath + addPath
}

// IsFileExist 文件是否存在
func IsFileExist(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}

	// 文件不存在
	if os.IsNotExist(err) {
		return false
	}

	// 其他类型错误，不确定是否存在
	return false
}

// ReadFile 读取文件
func ReadFile(filePath string) []byte {
	_file, err := os.Open(filePath)
	if err != nil {
		fmt.Println("文件读取失败!! err-->", err)
		return nil
	}
	defer _file.Close()

	fileData, err := ioutil.ReadAll(_file)
	if err != nil {
		fmt.Println("文件读取失败!! err-->", err)
		return nil
	}

	return fileData
}

// DelFile 删除文件
func DelFile(path string) {
	// 目标不存在
	if !IsFileExist(path) {
		return
	}

	err := os.Remove(path)
	if err != nil {
		fmt.Printf("DelFile fail path: %s \n", path)
	}

}

func ScanDir(path string) []string {
	_files, err := ioutil.ReadDir(path)
	if err != nil {
		return nil
	}

	// 没有文件
	if len(_files) == 0 {
		return nil
	}

	var fileList []string
	for _, _file := range _files {
		// 排除文件夹
		if _file.IsDir() {
			continue
		}
		fileList = append(fileList, _file.Name())
	}

	return fileList
}

// CreateFolder 创建文件夹
func CreateFolder(path string) error {
	// 已存在
	if IsFileExist(path) {
		return nil
	}

	err := os.Mkdir(path, os.ModePerm)

	return err

}

// CreateFile 创建文件
func CreateFile(path string) error {
	// 已存在
	if IsFileExist(path) {
		return nil
	}

	_file, err := os.Create(path)
	_file.Close()

	return err

}

func OpenFile(path string, flag int) (*os.File, error) {
	if flag != os.O_RDONLY {
		// 不存在就创建
		CreateFile(path)
	}

	_file, err := os.OpenFile(path, flag, os.ModePerm)
	if err != nil {
		return nil, err
	}

	return _file, err
}

/*
----------------------------------------- 获取项目根目录 -----------------------------------------
*/

// getRootPath 获取项目根路径
func getRootPath() string {
	dir := getCurAbsPathByGoBuild()
	if strings.Contains(dir, getTmpDir()) {
		dir = getCurAbsPathByGoRun()
	}
	res, _ := filepath.Split(dir)
	return res
}

// 获取系统临时目录
func getTmpDir() string {
	dir := os.Getenv("TEMP")
	if dir == "" {
		dir = os.Getenv("TMP")
	}
	res, _ := filepath.EvalSymlinks(dir)
	return res
}

// 获取当前执行文件的绝对路径(go build)
func getCurAbsPathByGoBuild() string {
	exePath, _ := os.Executable()
	res, _ := filepath.EvalSymlinks(filepath.Dir(exePath))
	return res
}

// 获取当前执行文件的绝对路径(go run)
func getCurAbsPathByGoRun() string {
	var abPath string
	_, filename, _, ok := runtime.Caller(0)
	if ok {
		abPath = path.Dir(filename)
	}

	return abPath
}

/*
----------------------------------------- 获取项目根目录 ----------------------------------------- end
*/
