package chat

import (
	g "gogame/game"
	"gogame/pb"
	"gogame/server/rpcx"
)

func (h *Api) SendMsgCheck(args *pb.SendMsgArgs, res *rpcx.Response) {
	res.IsCheck = 1
	res.S = 1

	_content := args.Content
	// 发送的消息不可为空
	if _content == "" {
		res.S = -1
		res.ErrorMsg = g.L("chat_error_1")
		return
	}

	_maxContentLen := 1000
	// 超过聊天字数限制
	if len(_content) > _maxContentLen {
		res.S = -2
		res.ErrorMsg = g.L("chat_error_2")
		return
	}

	_content = g.FilterStr(_content, false)  // 去掉特殊字符
	// 存在非法字符
	if _content == "" {
		res.S = -3
		res.ErrorMsg = g.L("chat_error_3")
		return
	}
	args.Content = _content

	// 你已被系统禁言，无法发言  TODO
	if !true {
		res.S = -4
		res.ErrorMsg = g.L("chat_error_4")
		return
	}

	_toUid := args.ToUid
	// 1系统聊天 2世界聊天 3私聊 4公会聊天 5跨服聊天 6跑马灯
	switch args.MsgType {
	case 1:
	case 2:
		_toUid = ""
	case 3:
		// 私聊必须传对方uid
		if _toUid == "" {
			res.S = -301
			res.ErrorMsg = g.L("chat_error_301")
			return
		}

		_toGud := g.GetGud(_toUid)
		// 玩家不存在
		if _toGud.Uid == "" {
			res.S = -302
			res.ErrorMsg = g.L("chat_error_302")
			return
		}
	case 4:
	case 5:
	default:
		// 不支持的消息类型
		res.S = -5
		res.ErrorMsg = g.L("chat_error_5")
		return
	}

}

// SendMsg 发送聊天消息
func (h *Api) SendMsg(request *rpcx.Request, args *pb.SendMsgArgs, res *rpcx.Response) {
	h.SendMsgCheck(args, res)
	// 条件检测不通过
	if res.S != 1 {
		return
	}

	g.M.Chat.SendMessage(request.Uid, args)

}
