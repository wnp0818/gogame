package redis

import (
	"context"
	"github.com/go-redis/redis/v8"
	"time"
)

// IRedis 弃用，用redis.Cmdable的
type IRedis interface {
	FlushAll(ctx context.Context) *redis.StatusCmd      // FlushAll 清除所有key 阻塞
	FlushAllAsync(ctx context.Context) *redis.StatusCmd // FlushAllAsync 清除所有key 非阻塞

	Pipeline() redis.Pipeliner                 // Pipeline 管道
	Ping(ctx context.Context) *redis.StatusCmd // Ping

	/*
		hash操作
	*/
	HSet(ctx context.Context, key string, values ...interface{}) *redis.IntCmd
	HGetAll(ctx context.Context, key string) *redis.StringStringMapCmd           // HGetAll 获取哈希key的所有kv
	HMSet(ctx context.Context, key string, values ...interface{}) *redis.BoolCmd // HMSet 设置哈希key的多个值
	HDel(ctx context.Context, key string, fields ...string) *redis.IntCmd        // HMDel 设置哈希key的多个值

	/*
		string操作
	*/
	Del(ctx context.Context, keys ...string) *redis.IntCmd                                   // Del 删除key
	Get(ctx context.Context, key string) *redis.StringCmd                                    // Get 获取值
	Set(ctx context.Context, key string, val any, expiration time.Duration) *redis.StatusCmd // Set 设置value
}
