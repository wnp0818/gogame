package taskoption

import (
	cfg "gogame/game/manage/config/structs"
	"gogame/pb"
)

/*
	任务模块option
*/

type FmtTaskData struct {
	TaskCon *cfg.GameTaskData
	NVal    int32
	Uid     string
}

type SetAddTask struct {
	TaskCon    *cfg.GameTaskData
	IsInsert   bool
	InsertData *pb.ModelTask
}
