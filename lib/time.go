package lib

import (
	"fmt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type TimeStruct struct {
	goCTime  string
	goCTime2 string
}

var Time = TimeStruct{
	goCTime:  "2006-01-02 15:04:05",
	goCTime2: "2006-01-02",
}

// Zero 获取当前0点时间戳
func (t TimeStruct) Zero(nt int64) int64 {
	_date := time.Unix(nt, 0).Format(t.goCTime2)
	_t, _ := time.ParseInLocation(t.goCTime2, _date, time.Local)
	return _t.Unix()
}

// Now 获取当前时间戳
func (t TimeStruct) Now() int64 {
	return time.Now().Unix()
}

// NowMillisecond 获取当前时间戳毫秒
func (t TimeStruct) NowMillisecond() int64 {
	return time.Now().UnixMilli()
}

// GetWeekKey  获取周标识
func (t TimeStruct) GetWeekKey(nt ...int64) string {
	var timeObj time.Time
	if len(nt) == 0 {
		timeObj = time.Now()
	} else {
		timeObj = time.Unix(nt[0], 0)
	}
	year, week := timeObj.ISOWeek()
	return fmt.Sprintf("%d-%d", year, week)
}

// GetWeekDayString 获取当前周几 字符串
func (t TimeStruct) GetWeekDayString() string {
	_weekDayFlag := time.Now().Weekday()
	var _weekDay string
	switch _weekDayFlag {
	case time.Monday:
		_weekDay = "星期一"
	case time.Tuesday:
		_weekDay = "星期二"
	case time.Wednesday:
		_weekDay = "星期三"
	case time.Thursday:
		_weekDay = "星期四"
	case time.Friday:
		_weekDay = "星期五"
	case time.Saturday:
		_weekDay = "星期六"
	case time.Sunday:
		_weekDay = "星期天"
	}
	return _weekDay
}

// GetWeekDayInt 获取当前周几 int
func (t TimeStruct) GetWeekDayInt() int {
	_weekDayFlag := time.Now().Weekday()
	var _weekDay int
	switch _weekDayFlag {
	case time.Monday:
		_weekDay = 1
	case time.Tuesday:
		_weekDay = 2
	case time.Wednesday:
		_weekDay = 3
	case time.Thursday:
		_weekDay = 4
	case time.Friday:
		_weekDay = 5
	case time.Saturday:
		_weekDay = 6
	case time.Sunday:
		_weekDay = 7
	}
	return _weekDay
}

// Date 获取当前日期
func (t TimeStruct) Date() string {
	return time.Unix(t.Now(), 0).Format(t.goCTime)
}

// Now2Date 时间戳转日期
func (t TimeStruct) Now2Date(nt int64) string {
	return time.Unix(nt, 0).Format(t.goCTime)
}

// Date2Now 日期转时间戳
func (t TimeStruct) Date2Now(date string) int64 {
	_t, _ := time.ParseInLocation(t.goCTime, date, time.Local)
	return _t.Unix()
}

// TTLTime 获取mongodb用过期时间
func (t TimeStruct) TTLTime() primitive.DateTime {
	_date := time.Unix(t.Now(), 0)
	return primitive.NewDateTimeFromTime(_date)
}
