package user

import (
	g "gogame/game"
	"gogame/pb"
	"gogame/server/rpcx"
)

func (h *Api) LvUpCheck(args *pb.LvUpArgs, res *rpcx.Response) {
	res.IsCheck = 1
	res.S = 1

}

// LvUp 玩家升级
func (h *Api) LvUp(request *rpcx.Request, args *pb.LvUpArgs, res *rpcx.Response) {
	h.LvUpCheck(args, res)
	// 条件检测不通过
	if res.S != 1 {
		return
	}

	_gud := g.GetGud(request.Uid)
	// 修改玩家名
	_setData := map[string]any{
		"lv": _gud.Lv + 1,
	}
	g.DATA.UserInfo.UpdateUserInfo(request.Uid, _setData)

}
