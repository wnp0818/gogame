package hero

import (
	"gogame/game/module/test"
	"gogame/pb"
)

func ApiGetList() {
	Args := &pb.HeroGetListArgs{}
	Hero.GetList(test.Request, Args, test.Response)
}
