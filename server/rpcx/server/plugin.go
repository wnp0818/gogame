package server

import (
	"fmt"
	"github.com/rcrowley/go-metrics"
	"github.com/rpcxio/rpcx-etcd/serverplugin"
	"gogame/gameconfig"
	"gogame/logger"
	"time"
)

// AddPlugin 中间件处理
func AddPlugin(s *RpcXServer) {
	// etcd
	AddEtcdPlugin(s)
	// conn close
	s.RpcServer.Plugins.Add(s)
}

// AddEtcdPlugin etcd
func AddEtcdPlugin(s *RpcXServer) {
	r := &serverplugin.EtcdV3RegisterPlugin{
		ServiceAddress: s.GetAddress(),
		EtcdServers:    gameconfig.ServerConfig.EtcdUrl,
		BasePath:       s.BasePath,
		Metrics:        metrics.NewRegistry(),
		UpdateInterval: time.Minute,
	}

	err := r.Start()
	if err != nil {
		logger.WorkerLog.Warn(fmt.Sprintf("AddEtcdPlugin err->%s", err))
	}
	s.RpcServer.Plugins.Add(r)
}
