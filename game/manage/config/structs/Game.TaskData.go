//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

package cfg

import "errors"

type GameTaskData struct {
    Taskid int32
    Qianzhi int32
    Type int32
    Stype int32
    Intr string
    Prize []*Gameatn
    Pval int32
    Cond int32
    Openlv int32
}

const TypeId_GameTaskData = 1722603851

func (*GameTaskData) GetTypeId() int32 {
    return 1722603851
}

func (_v *GameTaskData)Deserialize(_buf map[string]interface{}) (err error) {
    { var _ok_ bool; var _tempNum_ float64; if _tempNum_, _ok_ = _buf["taskid"].(float64); !_ok_ { err = errors.New("taskid error"); return }; _v.Taskid = int32(_tempNum_) }
    { var _ok_ bool; var _tempNum_ float64; if _tempNum_, _ok_ = _buf["qianzhi"].(float64); !_ok_ { err = errors.New("qianzhi error"); return }; _v.Qianzhi = int32(_tempNum_) }
    { var _ok_ bool; var _tempNum_ float64; if _tempNum_, _ok_ = _buf["type"].(float64); !_ok_ { err = errors.New("type error"); return }; _v.Type = int32(_tempNum_) }
    { var _ok_ bool; var _tempNum_ float64; if _tempNum_, _ok_ = _buf["stype"].(float64); !_ok_ { err = errors.New("stype error"); return }; _v.Stype = int32(_tempNum_) }
    { var _ok_ bool; if _v.Intr, _ok_ = _buf["intr"].(string); !_ok_ { err = errors.New("intr error"); return } }
     {
                var _arr_ []interface{}
                var _ok_ bool
                if _arr_, _ok_ = _buf["prize"].([]interface{}); !_ok_ { err = errors.New("prize error"); return }

                _v.Prize = make([]*Gameatn, 0, len(_arr_))
                
                for _, _e_ := range _arr_ {
                    var _list_v_ *Gameatn
                    { var _ok_ bool; var _x_ map[string]interface{}; if _x_, _ok_ = _e_.(map[string]interface{}); !_ok_ { err = errors.New("_list_v_ error"); return }; if _list_v_, err = DeserializeGameatn(_x_); err != nil { return } }
                    _v.Prize = append(_v.Prize, _list_v_)
                }
            }

    { var _ok_ bool; var _tempNum_ float64; if _tempNum_, _ok_ = _buf["pval"].(float64); !_ok_ { err = errors.New("pval error"); return }; _v.Pval = int32(_tempNum_) }
    { var _ok_ bool; var _tempNum_ float64; if _tempNum_, _ok_ = _buf["cond"].(float64); !_ok_ { err = errors.New("cond error"); return }; _v.Cond = int32(_tempNum_) }
    { var _ok_ bool; var _tempNum_ float64; if _tempNum_, _ok_ = _buf["openlv"].(float64); !_ok_ { err = errors.New("openlv error"); return }; _v.Openlv = int32(_tempNum_) }
    return
}

func DeserializeGameTaskData(_buf map[string]interface{}) (*GameTaskData, error) {
    v := &GameTaskData{}
    if err := v.Deserialize(_buf); err == nil {
        return v, nil
    } else {
        return nil, err
    }
}
