package config

import "strings"

type Other struct {
	*JsonHandle
	// 预处理字段定义区域---start

	// 预处理字段定义区域---end
}

func init() {
	config := &Other{
		JsonHandle: NewJsonHandle("Other"),
	}
	Manage.configs.Other = config
	RegisterConfigInterface(config.Name, config)
}

// Load 加载配置
func (self *Other) Load(jsonData any) error {
	err := self.JsonHandle.Load(jsonData)
	if err != nil {
		return err
	}
	self.PreConfig()
	return nil
}

// PreConfig 配置预处理
func (self *Other) PreConfig() {

}

// ------------------------ 以上文件内容自动生成,请勿随便修改! ------------------------
// ------------------------ 下方、定义配置diy方法 ------------------------

// GetData 获取敏感词列表
func (self *Other) GetData() []string {
	return strings.Split(self.JsonData["word"].(string), "|")
}
