package cross_user

import (
	g "gogame/game"
	"gogame/pb"
	"gogame/server/rpcx"
)

func (h *Api) GetInfoCheck(args *pb.UserGetInfoArgs, res *rpcx.Response) *pb.ModelUser {
	res.IsCheck = 1
	res.S = 1

	_userInfo := g.DATA.CrossUserInfo.GetGud(args.Uid)
	// 玩家不存在
	if _userInfo.Uid == "" {
		res.S = -1
		res.ErrorMsg = g.L("user_error_3")
		return _userInfo
	}

	return _userInfo

}

// GetInfo 获取玩家数据
func (h *Api) GetInfo(request *rpcx.Request, args *pb.UserGetInfoArgs, res *rpcx.Response) {
	_userInfo := h.GetInfoCheck(args, res)
	// 条件检测不通过
	if res.S != 1 {
		return
	}

	res.SendMsg(&pb.UserGetInfoRes{
		UserInfo: _userInfo,
	})

}
