package main

import (
	"gogame/game/module"
	"gogame/gameservice"
	_ "gogame/gameservice/worker"
)

func main() {
	// 启动worker
	gameservice.RunWorker()
	// 接口注册
	module.InitRoute()

	// 等待服务结束
	gameservice.WaitStop()
}
