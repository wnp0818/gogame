package datatype

import "sync"

/*
模拟python.Set实现的go版本
*/

type Set struct {
	sync.RWMutex
	mapData map[any]struct{}
}

func NewSet() *Set {
	return &Set{
		mapData: make(map[any]struct{}),
	}
}

// Get 获取一个元素
func (s *Set) Get(data any) any {
	s.RLock()
	defer s.RUnlock()
	_val, _ok := s.mapData[data]
	if !_ok {
		return nil
	}

	return _val
}

// Add 增加一个元素
func (s *Set) Add(data any) {
	s.Lock()
	defer s.Unlock()
	s.mapData[data] = struct{}{}
}

// Remove 移除一个元素
func (s *Set) Remove(data any) {
	s.Lock()
	defer s.Unlock()
	delete(s.mapData, data)
}

// Clear 清空set
func (s *Set) Clear() {
	s.Lock()
	defer s.Unlock()
	s.mapData = make(map[any]struct{})
}

// In 元素是否存在
func (s *Set) In(data any) bool {
	s.RLock()
	defer s.RUnlock()
	_, ok := s.mapData[data]
	return ok
}

// Len 集合长度
func (s *Set) Len() int64 {
	s.RLock()
	defer s.RUnlock()
	_len := int64(len(s.mapData))
	return _len
}
