package mongo

import (
	"gogame/gameconfig"
)

// InitOptions 初始化mongo配置
func InitOptions(ver string) gameconfig.MongoDb {
	switch ver {
	case "cross":
		return gameconfig.CrossServer.MongoDb
	default:
		return gameconfig.ServerConfig.MongoDb
	}

}
