package config

import cfg "gogame/game/manage/config/structs"

type Item struct {
	StructHandle
	*cfg.GameItem
	NewFunc func(_buf []map[string]any) (*cfg.GameItem, error) // 数据生成方法

	// 预处理字段定义区域---start

	// 预处理字段定义区域---end
}

func init() {
	config := &Item{
		StructHandle: NewStructHandle("game_Item"),
		NewFunc:      cfg.NewGameItem,
	}
	Manage.configs.Item = config
	RegisterConfigInterface(config.Name, config)
}

// Load 加载配置
func (self *Item) Load(jsonData any) error {
	data, err := self.NewFunc(jsonData.([]map[string]any))
	if err != nil {
		return err
	}
	self.GameItem = data
	self.PreConfig()
	return nil
}

// PreConfig 配置预处理
func (self *Item) PreConfig() {

}

// ------------------------ 以上文件内容自动生成,请勿随便修改! ------------------------
// ------------------------ 下方、定义配置diy方法 ------------------------

// GetListByColor 获取 x color以上的道具
func (self *Item) GetListByColor(star int32) []*cfg.GameItemData {
	_itemList := make([]*cfg.GameItemData, 0)
	for _, i := range self.GetDataList() {
		// color不足
		if i.Color < star {
			continue
		}
		_itemList = append(_itemList, i)
	}
	return _itemList
}
