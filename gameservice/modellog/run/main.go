package main

import (
	"gogame/gameservice"
	_ "gogame/gameservice/modellog"
)

func main() {
	// 启动modelLog
	gameservice.RunModelLog()

	// 等待服务结束
	gameservice.WaitStop()
}
