package model

import (
	"gogame/lib"
	"gogame/pb"
	"time"
)

type CrossUserInfo struct {
	*ModelInfo
}

func init() {
	model := &CrossUserInfo{
		ModelInfo: NewModelInfo("cross_userinfo", "uid", SetExpireTime(time.Hour*1)),
	}
	Manage.models.CrossUserInfo = model
}

// GetGud 获取玩家gud
func (self *CrossUserInfo) GetGud(uid string) *pb.ModelUser {
	_gud := &pb.ModelUser{}
	self.Get(uid, _gud)

	return _gud
}

// UpdateUserInfo 更新玩家数据
func (self *CrossUserInfo) UpdateUserInfo(uid string, setData map[string]any) {
	self.Set(uid, uid, setData)
}

// PushGud 上传更新玩家数据
func (self *CrossUserInfo) PushGud(uid string, gud *pb.ModelUser) {
	self.AddModelBase(uid)
	if self.GetGud(uid).Uid == "" {
		self.InsertOne(uid, gud)
	} else {
		self.Set(uid, uid, lib.Convert.Struct2Map(gud))
	}
}
