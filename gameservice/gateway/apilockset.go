package gateway

import (
	"sync"
)

/*
接口锁实现
*/

type ApiLockSet struct {
	sync.RWMutex
	mapData map[string]int64
}

func NewApiLockSet() *ApiLockSet {
	return &ApiLockSet{
		mapData: make(map[string]int64),
	}
}

// GetLockTime 获取一个锁的时间
func (s *ApiLockSet) GetLockTime(api string) int64 {
	s.RLock()
	defer s.RUnlock()
	_val, _ := s.mapData[api]

	return _val
}

// SetLockTime 设置锁时间
func (s *ApiLockSet) SetLockTime(api string, lockTime int64) {
	s.Lock()
	defer s.Unlock()
	s.mapData[api] = lockTime
	//logger.Print("设置锁---<%s", api)
}

// DelLock 删除锁
func (s *ApiLockSet) DelLock(api string) {
	s.Lock()
	defer s.Unlock()
	delete(s.mapData, api)
	//logger.Print("删除锁---<%s", api)

}

// Clear 清空所有锁
func (s *ApiLockSet) Clear() {
	s.Lock()
	defer s.Unlock()
	s.mapData = make(map[string]int64)
}

// In 锁是否存在
func (s *ApiLockSet) In(api string) bool {
	s.RLock()
	defer s.RUnlock()
	_, ok := s.mapData[api]
	return ok
}

// Len 当前有多少锁
func (s *ApiLockSet) Len() int64 {
	s.RLock()
	defer s.RUnlock()
	_len := int64(len(s.mapData))
	return _len
}
