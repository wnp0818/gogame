package module

import (
	"fmt"
	_ "gogame/game/module/chat"
	_ "gogame/game/module/cross_user"
	_ "gogame/game/module/hero"
	_ "gogame/game/module/item"
	_ "gogame/game/module/task"
	_ "gogame/game/module/user"
)

func InitRoute() {
	fmt.Println()
}
