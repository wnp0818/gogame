package tcp

import (
	"bufio"
	"io"
	"net"
)

type ConnChannelInterface interface {
	io.ReadWriteCloser

	Flush() error
	LocalAddr() net.Addr
	RemoteAddr() net.Addr
}

type ConnChannel struct {
	conn   net.Conn
	chanRW *bufio.ReadWriter
}

func NewConnChannel(conn net.Conn) *ConnChannel {
	c := &ConnChannel{
		conn:   conn,
		chanRW: bufio.NewReadWriter(bufio.NewReader(conn), bufio.NewWriter(conn)),
	}

	return c
}

func (c *ConnChannel) Close() error {
	return c.conn.Close()
}
func (c *ConnChannel) Read(p []byte) (n int, err error) {
	return c.chanRW.Read(p)
}
func (c *ConnChannel) Write(p []byte) (n int, err error) {
	return c.chanRW.Write(p)
}

func (c *ConnChannel) Flush() error {
	return c.chanRW.Flush()
}
func (c *ConnChannel) LocalAddr() net.Addr {
	return c.conn.LocalAddr()
}
func (c *ConnChannel) RemoteAddr() net.Addr {
	return c.conn.RemoteAddr()
}
