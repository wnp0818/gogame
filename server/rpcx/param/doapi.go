package param

type Args struct {
	Uid        string
	Rid        string
	GatewayUrl string
	P          string
	D          []any
}

type Res struct {
	S     int64
	Error string `json:"error"`
	Data  map[string]any
}
