package item

import (
	"gogame/game/manage/module"
	"gogame/game/module/base"
	"gogame/gameservice/worker"
)

type ModuleItem struct {
	*base.ModuleBase
	Api
}

var Item *ModuleItem

func init() {
	Item = &ModuleItem{
		ModuleBase: base.NewModuleBase(),
	}

	_moduleName := "item"
	// 路由注册
	worker.Register(_moduleName, Item)
	// 模块注册
	module.Manage.Item = Item
}
