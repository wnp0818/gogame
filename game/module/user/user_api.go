package user

import (
	"gogame/game/module/test"
	"gogame/pb"
)

func ApiLogin() {
	Args := &pb.LoginArgs{
		BindUid: "wnp001",
		Sid:     0,
	}
	User.Login(test.Request, Args, test.Response)
	test.Request.Uid = test.Response.Uid
}

func ApiChangeName() {
	Args := &pb.ChangeNameArgs{
		Name: "你王哥66",
	}
	User.ChangeName(test.Request, Args, test.Response)
}
