package task

import (
	g "gogame/game"
	"gogame/pb"
	"gogame/server/rpcx"
)

func (h *Api) OpenCheck(args *pb.TaskOpenArgs, res *rpcx.Response) {
	res.S = 1
	res.IsCheck = 1

	_taskType := args.TaskType
	// 任务类型不存在
	if !g.GC.Task().HasType(_taskType) {
		res.S = -1
		res.ErrorMsg = g.L("task_error_1")
		return
	}

}

// Open 获取某一类型的所有任务
func (h *Api) Open(request *rpcx.Request, args *pb.TaskOpenArgs, res *rpcx.Response) {
	h.OpenCheck(args, res)

	res.SendMsg(&pb.TaskOpenRes{
		TaskList: g.DATA.Task.GetListByTaskType(request.Uid, args.TaskType),
	})
}
