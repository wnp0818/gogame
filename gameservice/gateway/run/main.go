package main

import (
	"gogame/gameservice"
	_ "gogame/gameservice/gateway"
)

func main() {
	// 启动gateway
	gameservice.RunGateway()

	// 等待服务结束
	gameservice.WaitStop()
}
