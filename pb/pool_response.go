package pb

import "sync"

/*
 ---------- pool Response ----------
*/
type responsePool struct {
	pool *sync.Pool
}

var ResponsePool = &responsePool{
	pool: &sync.Pool{
		New: func() any {
			return &Response{}
		},
	},
}

func (p *responsePool) Get() *Response {
	return p.pool.Get().(*Response)
}

func (p *responsePool) Put(response *Response) {
	response.S = 0
	response.ErrorMsg = ""
	response.ResponseApi = ""
	response.Data = nil
	response.StringMsg = nil

	p.pool.Put(response)
}

func NewResponse() *Response {
	return ResponsePool.Get()
}

// PutResponse 资源回收
func PutResponse(response *Response) {
	ResponsePool.Put(response)
}

/*
 ---------- pool Response ---------- end
*/
