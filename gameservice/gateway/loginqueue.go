package gateway

import (
	"gogame/lib/datatype"
)

/*
简单的gateway单进程内存登录队列实现，主要是为了避免瞬时服务器压力
*/

type loginQueue struct {
	loginUsers *datatype.Set
}

func NewLoginQueue() *loginQueue {
	return &loginQueue{
		loginUsers: datatype.NewSet(),
	}
}

// WillLogin 玩家登陆
func (l *loginQueue) WillLogin(conn *GatewayApp) int64 {
	conn.InLoginQueue = true
	_delayTime := l.GetDelayTime()
	l.AddUser(conn.Id)

	return _delayTime
}

// RoleCreateOver 玩家创建完成
func (l *loginQueue) RoleCreateOver(conn *GatewayApp) {
	// 不在队列中
	if !conn.InLoginQueue {
		return
	}
	conn.InLoginQueue = false
	l.DelUser(conn.Id)
}

// GetDelayTime 获取等待时长
func (l *loginQueue) GetDelayTime() int64 {
	return l.loginUsers.Len() / 5.0 * 1000
}

// AddUser 增加一个玩家
func (l *loginQueue) AddUser(data any) {
	l.loginUsers.Add(data)
}

// DelUser 移除一个玩家
func (l *loginQueue) DelUser(data any) {
	l.loginUsers.Remove(data)
}
